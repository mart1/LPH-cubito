"""
Cubito
"""

import arcade
import random
import cubito

# Screen title and size
SCREEN_TITLE = "Cubito"
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 500

# Margin between mat & screen side
VERTICAL_MARGIN_PERCENT = 0.10
HORIZONTAL_MARGIN_PERCENT = 0.10

# Token size
TOKEN_SCALE = 0.5
TOKEN_HEIGHT = 75 * TOKEN_SCALE
TOKEN_WIDTH = 75 * TOKEN_SCALE

# Space between tokens
X_SPACING_TOKEN = TOKEN_WIDTH + TOKEN_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_TOKEN = TOKEN_HEIGHT + TOKEN_HEIGHT * VERTICAL_MARGIN_PERCENT

# List of token types
TOKEN_TYPES = ["FORDWARD", "RIGHT", "LEFT", "PAUSE", "FUNCTION"]

COLOR_MAT = arcade.color.AO

# Mat size
MAT_PERCENT_OVERSIZE = 1.25
MAT_HEIGHT = int(TOKEN_HEIGHT * MAT_PERCENT_OVERSIZE)
MAT_WIDTH = int(TOKEN_WIDTH * MAT_PERCENT_OVERSIZE)

# Number of column & row mat
MAT_COLUMN = 4
MAT_ROW = 4
MAT_FUNCTION_ROW = 2

# Space between mats
X_SPACING_MAT = MAT_WIDTH + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_MAT = MAT_HEIGHT + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Top for mats
TOP_Y = SCREEN_HEIGHT - MAT_HEIGHT - MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Bottom for tokens
BOTTOM_Y = MAT_HEIGHT / 2 + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Start from left side
START_X = MAT_WIDTH / 2 + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT



class Token(arcade.Sprite):
    """ Card sprite """

    token_image = {"FORDWARD" : ":resources:onscreen_controls/shaded_dark/up.png",
        "RIGHT" : ":resources:onscreen_controls/shaded_dark/right.png",
        "LEFT" : ":resources:onscreen_controls/shaded_dark/left.png",
        "PAUSE" : ":resources:onscreen_controls/shaded_dark/pause_square.png",
        "FUNCTION" : ":resources:onscreen_controls/shaded_dark/hamburger.png",
        }

    def __init__(self, type_token,scale=0.5):
        """ Card constructor """
        
        self.type_token = type_token
        self.image_file_name = Token.token_image[type_token]
        # Call the parent
        super().__init__(self.image_file_name, scale, hit_box_algorithm="None")
        
class Mat(arcade.SpriteSolidColor):
    """ Token sprite """

    def __init__(self, height, width, color = arcade.color.AMARANTH):
        """ Token constructor """

        # Call the parent
        super().__init__(width, height, color)

class Cubito(arcade.Window):
    """Main application class"""

    def __init__(self):
        # Init parent class
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        # Set background color
        arcade.set_background_color(arcade.color.AMAZON)

        # List of tokens
        self.token_list = None

        # Hold token
        self.held_token = None

        # Origin pos for hold token
        self.held_token_original_position = None

        # List of mats
        self.mat_list = None

        # List of mats function
        self.mat_function_list = None
        
        # List of cards we are dragging with the mouse
        self.held_tokens = None

        # Original location of cards we are dragging with the mouse in case
        # they have to go back.
        self.held_tokens_original_position = None

    def setup(self):
        """Set up the game"""
    
        self.token_list = arcade.SpriteList()
        for i in range(9):
            token = Token(type_token=TOKEN_TYPES[i%5])
            token.position = START_X, BOTTOM_Y
            self.token_list.append(token)
        # List of cards we are dragging with the mouse
        self.held_tokens = []

        self.mat_list = arcade.SpriteList()
        for j in range(4):
            for i in range(4):                
                mat = Mat(MAT_HEIGHT, MAT_WIDTH, COLOR_MAT)
                mat.position = START_X + 50*i, TOP_Y - 50*j
                self.mat_list.append(mat)
        
        self.mat_function_list = arcade.SpriteList()
        for j in range(4) :
            for i in range(2) :
                mat_function = Mat(MAT_HEIGHT, MAT_WIDTH, COLOR_MAT)
                mat_function.position = START_X + 50*i, TOP_Y - 50*j
                self.mat_function_list.append(mat_function)

        # Original location of cards we are dragging with the mouse in case
        # they have to go back.
        self.held_tokens_original_position = []

    def on_draw(self):
        """Render the screen"""
        # Clear the screen
        arcade.start_render()
        self.mat_list.draw()
        self.token_list.draw()
        self.mat_function_list()

    def on_mouse_press(self, x, y, button, key_modifiers):
        """Called when the user presses a mouse button"""
       # Get list of cards we've clicked on
        tokens = arcade.get_sprites_at_point((x, y), self.token_list)

        # Have we clicked on a card?
        if len(tokens) > 0:

            # Might be a stack of cards, get the top one
            primary_token = tokens[-1]

            # All other cases, grab the face-up card we are clicking on
            self.held_tokens = [primary_token]
            # Save the position
            self.held_tokens_original_position = [self.held_tokens[0].position]
            
    def on_mouse_release(self, x : float, y : float , button : int, modifiers : int):
        """Called when the user presses a mouse button"""
        # If we don't have any cards, who cares
        if len(self.held_tokens) == 0:
            return
        
        # Find the closest pile, in case we are in contact with more than one
        mat, distance = arcade.get_closest_sprite(self.held_tokens[0], self.mat_list)
        reset_position = True

        # See if we are in contact with the closest pile
        if arcade.check_for_collision(self.held_tokens[0], mat):

            # For each held card, move it to the pile we dropped on
            for i, dropped_token in enumerate(self.held_tokens):
                # Move cards to proper position
                dropped_token.position = mat.center_x, mat.center_y

            # Success, don't reset position of cards
            reset_position = False

            # Release on top play pile? And only one card held?
        if reset_position:
            # Where-ever we were dropped, it wasn't valid. Reset the each card's position
            # to its original spot.
            for mat_index, token in enumerate(self.held_tokens):
                token.position = self.held_tokens_original_position[mat_index]

        # We are no longer holding cards
        self.held_tokens = []

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the user moves the mouse"""

        # If we are holding cards, move them with the mouse
        for token in self.held_tokens:
            token.center_x += dx
            token.center_y += dy

    def on_key_press(self, symbol, modifiers):
        """Called when the user presses key"""
        if symbol == arcade.key.L :
            for mat in self.mat_list :
                token, distance = arcade.get_closest_sprite(mat ,self.token_list)
                if arcade.check_for_collision(token, mat) :
                    print(token.type_token)
                    if token.type_token == "FORDWARD" :
                        cubito.FORDWARD()
                    if token.type_token == "LEFT" :
                        cubito.LEFT()
                    if token.type_token == "RIGHT" :
                        cubito.RIGHT()
                    if token.type_token == "PAUSE" :
                        cubito.PAUSE()

    def cubito(self, function=False):
        """Move cubito !"""

def main():
    """Main method"""
    window = Cubito()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
