import turtle

# Cell size
distance = 90

# Set background - ocean
turtle.bgpic("background.png")

# Set turtle cusor
turtle.pensize(10)
turtle.pencolor("gray")
turtle.color("red", "lightgreen")
turtle.shape("turtle")
turtle.shapesize(2)

# Goto to first cell
turtle.penup()
turtle.goto(-225,225)
turtle.pendown()


def FORDWARD():
    """
    Forward fonction
    """
    turtle.forward(distance)

def LEFT():
    """
    Turn left fonction
    """
    turtle.left(90)

def RIGHT():
    """
    Turn right fonction
    """
    turtle.right(90)

def PAUSE() :
    """
    Pause fonction
    """